<?php

class Admin extends CI_Controller {

    protected $currentUserId;
    public function __construct()
    {
      parent::__construct();
      $this->load->library('session');
      $this->load->library('authenticate');
      $this->load->library('viewengine');
      $this->load->model("Auth_Model");
      $this->load->model("Bank_Model");
      $this->load->model("Payment_Model");
      $this->load->model("Help_Model");
      $this->output->enable_profiler(FALSE);
      $this->authenticate->checkLogInState();
      $this->authenticate->verifyUserType();
      $this->currentUserId = $this->session->userdata['user_id'];
    }

	public function index (){
    $this->viewengine->_output(["admin/dashboard"]);
  }

  public function pair()
  {
    $helper = $this->Help_Model->getNewHelpers();
    $requester = $this->Help_Model->getNewConfirmPaid();
    $data['helper'] = $helper;
    $data['requester'] = $requester;
    $this->viewengine->_output(["admin/pair"], $data);

  }
}

?>
