<?php

class User extends CI_Controller {

    protected $currentUserId;
    public function __construct()
    {
      parent::__construct();
      $this->load->library('session');
      $this->load->library('authenticate');
      $this->load->library('viewengine');
      $this->load->model("Auth_Model");
      $this->load->model("Bank_Model");
      $this->load->model("Payment_Model");
      $this->output->enable_profiler(FALSE);
      $this->authenticate->checkLogInState();
      $this->authenticate->verifyUserType();
      $this->currentUserId = $this->session->userdata['user_id'];
    }

	public function index (){
    $data['user_details'] = $this->Auth_Model->getUserDetails($this->currentUserId);
    // print_r($this->Payment_Model->getNewUserToRecieve($this->currentUserId));
    // return;
    if (count($this->Payment_Model->getNewUserToRecieve($this->currentUserId))) {
      $data['has_new_payment'] = 1;
      $data['user_payment'] = $this->Payment_Model->getNewUserToRecieve($this->currentUserId);
      $data['total_paid'] = $this->Payment_Model->totalPaid($this->currentUserId);
      $data['total_recieved'] = $this->Payment_Model->totalRecieved($this->currentUserId);
      $this->viewengine->_output(["user/dashboard","user/payment_table"], $data);
    }
    elseif (count($this->Payment_Model->getNewUserToPay($this->currentUserId))) {
      $data['has_new_payment'] = 1;
      $data['user_payment'] = $this->Payment_Model->getNewUserToPay($this->currentUserId);
      $data['total_paid'] = $this->Payment_Model->totalPaid($this->currentUserId);
      $data['total_recieved'] = $this->Payment_Model->totalRecieved($this->currentUserId);
      $this->viewengine->_output(["user/dashboard","user/payment_table_receive"], $data);
    }
    else {
      $data['has_new_payment'] = 0;
      $data['total_paid'] = $this->Payment_Model->totalPaid($this->currentUserId);
      $data['total_recieved'] = $this->Payment_Model->totalRecieved($this->currentUserId);
      $this->viewengine->_output(["user/dashboard"], $data);
    }
	}

  public function bank()
  {
    $data['banks'] = $this->Bank_Model->getAllBanks();
    if (count($this->Bank_Model->getUserBank($this->currentUserId))) {
      $data['has_bank'] = 1;
    }
    else {
      $data['has_bank'] = 0;
    }
    $data['user_bank'] = $this->Bank_Model->getUserBank($this->currentUserId);
    if ($data['has_bank']) {
      $this->viewengine->_output("user/bank", $data);
    }
    else {
      $this->viewengine->_output("user/add_bank", $data);
    }
  }

  public function addbanksdetails()
  {
    $this->output->enable_profiler(FALSE);
    $bank_id = $this->input->post('bank_id');
    $account_name = $this->input->post('account_name');
    $account_number = $this->input->post('account_number');
    $insert = $this->Bank_Model->addUserBank($bank_id, $account_name, $account_number,$this->currentUserId);
    if ($insert) {
      print_r(json_encode([
        'status' => true,
        'message' => 'Bank Added Successfully'
      ]));
    }
    else {
      print_r(json_encode([
        'status' => false,
        'message' => 'Error adding bank details'
      ]));
    }
  }

  public function opes_pay_request()
  {
    $userBank = $this->Bank_Model->getUserBank($this->currentUserId);
    $getAccountTypes = $this->Auth_Model->getPackages();
    $getLastUserPackages = $this->Auth_Model->getLastUserPackages($this->currentUserId);
    echo "<pre>";
    foreach ($getAccountTypes as $value) {

    }
    print_r($userBank);
    print_r($getAccountTypes);
    print_r($getLastUserPackages[0]);
  }
  public function pay_request()
  {
    $this->output->enable_profiler(FALSE);
    $userBank = $this->Bank_Model->getUserBank($this->currentUserId);
    if (count($userBank)) {
      $data['has_bank'] = 1;
    }
    else {
      $data['has_bank'] = 0;
    }

    $getLastUserPackages = $this->Auth_Model->getLastUserPackages($this->currentUserId);
    if ($getLastUserPackages[0]['status']) {
      $data['has_pending_pay'] = 0;
    }
    elseif (!$getLastUserPackages[0]['status']) {
      $data['has_pending_pay'] = 1;
    }
    // print_r($data);
    $this->viewengine->_output("user/pay_request", $data);
  }

  public function make_pay_addition()
  {
    $this->output->enable_profiler(FALSE);
    $amount_to_pay = $this->input->post("amount_to_pay");
    $pay = $this->Auth_Model->add_new_user_payment($amount_to_pay, $this->currentUserId);
    if ($pay) {
      print_r(json_encode([
        'status' => false,
        'message' => 'Successfully'
      ]));
    }
    else {
      print_r(json_encode([
        'status' => false,
        'message' => 'Not Successful'
      ]));
    }
  }
}

?>
