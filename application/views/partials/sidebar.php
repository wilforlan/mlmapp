<div class="off-canvas-overlay" data-toggle="sidebar"></div>
<div class="sidebar-panel">
    <div class="brand"> <a href="javascript:;" data-toggle="sidebar" class="toggle-offscreen hidden-lg-up"><i class="material-icons">menu</i> </a>
        <a class="brand-logo">
          <!-- <img class="expanding-hidden" src="/images/logo.png" alt="">  -->
          <h3>Opes Family</h3>
        </a>
    </div>
    <div class="nav-profile dropdown">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
            <div class="user-image"><img src="/images/avatar.jpg" class="avatar img-circle" alt="user" title="user">
            </div>
            <div class="user-info expanding-hidden">
              <?php
              // print_r($user_details);
              echo $user_details->first_name. ' ' . $user_details->last_name;
              // echo "Williams Isaac";
              ?>
              <small class="bold">Opes Family</small>
            </div>
        </a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="javascript:;">Account Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:;">Help</a>
            <a class="dropdown-item" href="/auth/logout">Logout</a>
        </div>
    </div>
    <nav>
        <p class="nav-title">NAVIGATION</p>
        <ul class="nav">
          <li><a href="/"><i class="material-icons text-primary">home</i> <span>Home</span></a>
          </li>
          <?php if ($this->session->userdata['user_type'] == 0): ?>
            <li><a href="/user/bank"><i class="material-icons text-primary">account_balance_wallet</i> <span>Bank Details</span></a>
            </li>
            <li><a href="/user/pay_request"><i class="material-icons text-primary">receipt</i> <span>Request to Pay</span></a>
            </li>
            <!-- <li><a href="/user/report"><i class="material-icons text-primary">error</i> <span>Report Abuse</span></a>
            </li> -->
          <?php elseif($this->session->userdata['user_type'] == 1): ?>
            <li><a href="/admin/pair"><i class="material-icons text-primary">home</i> <span>Pair Users</span></a>
            </li>
          <?php endif; ?>
        </ul>
    </nav>
</div>
