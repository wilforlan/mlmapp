                    <div class="row">
                      <h6>Recent notifications</h6>
                      <ul class="list-group m-b-1">
                          <li class="list-group-item notification-bar-success">
                              <div href="#" class="notification-bar-icon">
                                  <div><i></i>
                                  </div>
                              </div>
                              <div class="notification-bar-details"><a href="#" class="notification-bar-title"><b style="margin-right: 10px;">Referral Link:</b>http://opesfamily.com/ref=<?php echo $user_details->hash_key; ?> </a><span class="text-muted">Earn 15% referral, and you will automatically recieve accumulated payment at the emd of the month </span>
                              </div>
                          </li>
                      </ul>
                      <ul class="list-group">
                          <li class="list-group-item notification-bar-fail">
                              <div href="#" class="notification-bar-icon">
                                  <div><i></i>
                                  </div>
                              </div>
                              <div class="notification-bar-details">
                                <span class="text-muted">You will receive payment in: </span>
                                <a href="#" class="notification-bar-title">
                                <div id="clock" data-countdown="" style="font-size: 30px; font-weight: bold;"></div>
                              </a>
                              </div>
                          </li>
                      </ul>
                  </div>
                  <div class="row" style="margin-top: 10px;">

                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card card-block">
                                <h5 class="m-b-0 v-align-middle text-overflow">
                                <span><?php echo ($total_paid->total_paid) ? $total_paid->total_paid : 0; ?></span></h5>
                                <div class="small text-overflow text-muted">Total Paid</div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card card-block">
                              <h5 class="m-b-0 v-align-middle text-overflow">
                              <span><?php echo ($total_recieved->total_recieved) ? $total_recieved->total_recieved : 0; ?></span></h5>
                              <div class="small text-overflow text-muted">Total Received</div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card card-block">
                                <h5 class="m-b-0 v-align-middle text-overflow">
                                <span>200,000</span></h5>
                                <div class="small text-overflow text-muted">Loops</div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="card card-block">
                                <h5 class="m-b-0 v-align-middle text-overflow">
                                <span>200,000</span></h5>
                                <div class="small text-overflow text-muted">Referral Bonus</div>
                            </div>
                        </div>
                      </div>
                      <?php if (!$has_new_payment): ?>
                        <div class="card">
                              <div class="card-header no-bg b-a-0"><h3>Your Payments</h3><span></span></div>
                              <div class="card-block">
                                <div class="alert alert-info">
                                  <h4>You have not being paired yet. Please check Back</h4>
                                </div>
                              </div>
                            </div>
                      <?php endif; ?>
