<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="Opes"><meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="Opes">
  <meta name="theme-color" content="#4C7FF0">
  <title>Opes Login</title>
<link rel="stylesheet" href="/styles/app.min.css">
</head>
<body>
  <div class="app no-padding no-footer layout-static">
    <div class="session-panel">
      <div class="session">
        <div class="session-content">
          <div class="card card-block form-layout">
            <form role="form" id="validate">
              <div class="text-xs-center m-b-3"><img src="/images/logo-icon.png" height="80" alt="" class="m-b-1">
                <h5>Opes Register</h5>
                <p class="text-muted">Fill the Form Below to Register</p>
                <div class="alert" id="error-messages" style="display:none">

                </div>
              </div>
              <fieldset class="form-group">
                <label for="username">Enter your Username</label>
                <input type="text" name="username" onfocusout="validateUsername(this.value)" class="form-control form-control-lg" id="username" placeholder="username" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="password">Choose password</label>
                <input type="password" name="password" class="form-control form-control-lg" id="password" placeholder="********" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Enter your Firstname</label>
                <input type="text" name="firstname" class="form-control form-control-lg" id="firstname" placeholder="firstname" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Enter your Lastname</label>
                <input type="text" name="lastname" class="form-control form-control-lg" id="lastname" placeholder="lastname" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Enter Phone Number</label>
                <input type="text" name="phone_number" onfocusout="validatePhone(this.value)" class="form-control form-control-lg" id="phone" placeholder="phone number" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Enter Email Address</label>
                <input type="text" name="email" onfocusout="validateEmail(this.value)" class="form-control form-control-lg" id="email" placeholder="email" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Enter Address</label>
                <input type="text" name="address" class="form-control form-control-lg" id="address" placeholder="address" required>
              </fieldset>
              <fieldset class="form-group">
                <label for="username">Choose Package</label>
                <select class="form-control form-control-lg" name="package" id="package">
                  <?php foreach ($packages as $package):?>
                  <option value="<?php echo $package->account_type_id; ?>"><?php echo $package->account_type_name; ?></option>
                <?php endforeach; ?>
                </select>
              </fieldset>
              <button class="btn btn-primary btn-block btn-lg" id="reg_user_in" type="submit">Register</button>
              <button class="btn btn-default btn-block btn-sm" disabled  style="background-color: white; height: 50px; display:none" id="reg_user_in_working" type="submit"><img src="/image/utils/loader.gif" style="height: 50px;" alt="Loading"/></button>
               <div class="divider">
                 <span>OR</span>
               </div>
           </form>
           </div>
         </div>
         <footer class="text-xs-center p-y-1">
           <p><button class="btn btn-danger btn-sm"><a href="extra-forgot.html">
             Forgot password? </a></button>
             &nbsp;&nbsp;·&nbsp;&nbsp; <button class="btn btn-info btn-sm"><a href="/auth/">Login</a></button>
           </p>
         </footer>
       </div>
     </div>
   </div>
   <script type="text/javascript">
   window.paceOptions = {
      document: true,
      eventLag: true,
      restartOnPushState: true,
      restartOnRequestAfter: true,
      ajax: {
        trackMethods: [ 'POST','GET']
      }
    };
</script>
<script src="/scripts/app.min.js"></script>
<script src="/scripts/Auth.js"></script>
<script src="/scripts/register.js"></script>
<!-- <script src="/scripts/RegisterValidators.js"></script> -->
<script src="/vendor/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
<!-- <script src="scripts/helpers/noty-defaults.js"></script> -->
<script src="/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script type="text/javascript">
  $('#validate').validate();


  </script>
</body>
</html>
