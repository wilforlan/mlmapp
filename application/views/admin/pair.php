<link rel="stylesheet" href="/styles/loaders.css"><div class="card" >
  <link rel="stylesheet" href="/bower_components/toastr/toastr.min.css">
  <link rel="stylesheet" href="/vendor/sweetalert/dist/sweetalert.css">
                  <div class="card">
                        <div class="card-header no-bg b-a-0"><h3>Pairing</h3></div>
                        <div class="card-block">
                          <div class="col-md-5">
                            <table class="table table-bordered datatable">
                                <thead>
                                    <tr>
                                        <th>Helper</th>
                                        <th>Helper Amount</th>

                                    </tr>
                                </thead>
                                <tbody>
                                  <?php

                                  $i = 0;
                                  foreach ($helper as $values):
                                      // var_dump($values[0], $values[1]);
                                      $i++;
                                  ?>
                                  <tr id="helperClass<?php echo $i; ?>">
                                    <td>
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" value="<?php echo $values->user_id; ?>" onclick="StateProvider(this.value, <?php echo $i; ?>,'h',<?php echo $values->amount; ?>,<?php echo $values->help_request_id; ?>)" class="custom-control-input"> <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $values->first_name.' '.$values->last_name; ?></span>
                                      </label>
                                    </td>
                                    <td>
                                      <?php echo $values->amount; ?>
                                    </td>

                                  </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                          </div>
                          <div class="col-md-1"></div>
                          <div class="col-md-4">
                            <table class="table table-bordered datatable">
                                <thead>
                                    <tr>
                                        <th>Requester</th>
                                        <th>Requester Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php

                                  $i = 0;
                                  foreach ($requester as $values):
                                      // var_dump($values[0], $values[1]);
                                      $i++;
                                  ?>
                                  <tr id="requesterClass<?php echo $i; ?>">
                                    <td>
                                      <label class="custom-control custom-checkbox">
                                        <input type="checkbox" value="<?php echo $values->user_id; ?>" onclick="StateProvider(this.value, <?php echo $i; ?>,'r',<?php echo $values->amount; ?>,<?php echo $values->comfirm_paid_users; ?>)" class="custom-control-input"> <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description"><?php echo $values->first_name.' '.$values->last_name; ?></span>
                                      </label>
                                    </td>
                                    <td>
                                      <?php echo $values->amount; ?>
                                    </td>

                                  </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                          </div>

                        </div>
                    </div>
                    <script src="/vendor/datatables/media/js/jquery.dataTables.js"></script>
                    <script src="/vendor/datatables/media/js/dataTables.bootstrap4.js"></script>
                    <script src="/bower_components/toastr/toastr.min.js" charset="utf-8"></script>
                    <script src="/vendor/sweetalert/dist/sweetalert.min.js"></script>
                    <!-- <script src="/scripts/ui/alert.js"></script> -->

                    <script type="text/javascript">
                        $('.datatable').DataTable({
                        //     'ajax': 'data/datatables-arrays.json'
                        });

                        let helpers = [];
                        let requesters = [];
                        let items = [];
                        let helper = {};
                        let requester = {};
                        var StateProvider = function (value, index, type,amount,db_id) {
                          function arrayObjectIndexOf(myArray, searchTerm, property) {
                            for(var i = 0, len = myArray.length; i < len; i++) {
                              if (myArray[i][property] === searchTerm) return i;
                            }
                            return -1;
                          }
                          if (type === 'r') {
                            requester.user_id = value;
                            requester.index = index;
                            requester.amount = amount;
                            requester.requester_db_id = db_id;
                            var checkRequester = ($.grep(items, function(element) {
                              return element.user_id == requester.user_id;
                            }))[0];
                            if (checkRequester == undefined) {
                              items.push(requester);
                              if (typeof helper.user_id != undefined) {
                                toastr.success('Ready to Pair.', 'Pair Complete', {timeOut: 5000})
                                triggerCompletePair(0,index);
                              }
                              else {
                                toastr.success('Please choose a Helper.', 'Requester Checked Successfully', {timeOut: 5000})
                              }
                            }
                            else {
                              var index = arrayObjectIndexOf(items, requester.user_id, value);
                              items.splice(index,1);
                              toastr.info('Choose a Requester.', 'A Requester Removed', {timeOut: 5000})
                            }
                          }
                          else if (type === 'h') {
                            helper.user_id = value;
                            helper.index = index;
                            helper.amount = amount;
                            helper.helper_db_id = db_id;
                            var checkHelper = ($.grep(items, function(element) {
                              return element.user_id == helper.user_id;
                            }))[0];
                            if (checkHelper == undefined) {
                              items.push(helper);
                              if (requester.user_id) {
                                toastr.success('Ready to Pair.', 'Pair Complete', {timeOut: 5000})
                                triggerCompletePair();
                              }
                              else {
                                toastr.success('Please choose a requester.', 'Helper Checked Successfully', {timeOut: 5000})
                              }
                            }
                            else {
                              var index = arrayObjectIndexOf(items, helper.user_id, value);
                              items.splice(index,1);
                              toastr.info('Choose a Helper.', 'A helper Removed', {timeOut: 5000})
                            }

                        }
                        console.log(items);
                        }

                        var triggerCompletePair = function(helperIndex, requesterIndex){
                          swal({
                            title:'Are you sure to Pair?',
                            text:'This will merge Helper to Requester!',
                            type:'warning',
                            showCancelButton:true,
                            confirmButtonColor:'#DD6B55',
                            confirmButtonText:'Yes, Merge!',
                            closeOnConfirm:false
                          },
                            function(){
                              var dataObject = {};
                              dataObject.helper_id = helper.user_id;
                              dataObject.helper_amount = helper.amount;
                              dataObject.reciever_id = requester.user_id;
                              dataObject.reciever_amount = requester.amount;
                              dataObject.helper_db_id = helper.helper_db_id;
                              dataObject.requester_db_id = requester.requester_db_id;
                              // console.log(dataObject);
                              // return;
                                $.ajax({
                                  method:"POST",
                                  url:"http://localhost:4000/payment/pair_users",
                                  data:dataObject,
                                  contentType:"application/x-www-form-urlencoded",
                                  success:function(result){
                                    var data = JSON.parse(result);
                                    if(data.status == true){
                                      $("#helperClass"+helper.index).hide('slow');
                                      $("#requesterClass"+requester.index).hide('slow');
                                      items.splice(0,items.length);
                                      swal('Merge Successful!','','success');
                                      }
                                    else {
                                      items.splice(0,items.length);
                                      swal('Error Merging!','','error');
                                    }
                                  },
                                  error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                    items.splice(0,items.length);
                                    swal('Error Merging!','','error');
                                  }
                                })
                            });
                        }

                         var ConfirmPay = function(id,type){
                          $('#confirm_pay').hide();
                          $('.loader').show('slow');
                          var dataObject = {};
                          dataObject.payment_id = id;
                          dataObject.trans_type = type;
                            $.ajax({
                              method:"POST",
                              url:"http://localhost:4000/payment/confirm_pay",
                              data:dataObject,
                              contentType:"application/x-www-form-urlencoded",
                              success:function(result){
                                var data = JSON.parse(result);
                                if(data.status == true){
                                  toastr.success('Payment Successfully Confirmed.', 'Success', {timeOut: 3000})
                                    setTimeout(function(){
                                      window.location.reload();
                                    }, 3000)
                                  }
                                else {
                                  toastr.error('Could not confirm payment, Please try again.', 'Erorr Confirming Payment', {timeOut: 5000})
                                  $('#confirm_pay').show();
                                  $('.loader').hide();
                                }
                              },
                              error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log(textStatus);
                                console.log(errorThrown);
                                toastr.error('Please Try again.', 'An error occured', {timeOut: 5000})
                                $('#confirm_pay').show();
                                $('.loader').hide();
                              }
                            })
                        }
                    </script>
