<div class="row" style="margin-top: 10px;">
  <div class="card">
    <div class="card-header no-bg b-a-0"><h3>New Registered</h3><span></span></div>
    <div class="card-block">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#one" role="tab">Bronze</a>
        </li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#two" role="tab">Silver</a>
        </li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#three" role="tab">Gold</a>
        </li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#four" role="tab">This is tab four</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="one" role="tabpanel">
          <p>Bronze</p>
        </div>
        <div class="tab-pane" id="two" role="tabpanel">
          <p>Silver</p>
        </div>
        <div class="tab-pane" id="three" role="tabpanel">
          <p>Gold</p>
        </div>
        <div class="tab-pane" id="four" role="tabpanel">
          <p>Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
        </div>
      </div>
    </div>
  </div>
</div>
