<?php
class Payment_model extends CI_Model {

  function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->load->library('session');
  }

  public function getNewUserToRecieve($user_id)
  {
    $this->db->select('payment.*, user_details.first_name,user_details.last_name, user_details.user_id');
    $this->db->from('payment');
    $querySelector = 'payment.reciever_id ='.$user_id.' AND payment.status = 0 OR payment.status = 2';
    $this->db->join('user_details', 'user_details.user_id = payment.payer_id','inner');
    $this->db->where($querySelector);
    $details = $this->db->get();
    return $details->result();
  }

  public function getNewUserToPay($user_id)
  {
    $this->db->select('payment.*, user_details.first_name,user_details.last_name, user_details.user_id, user_banks.*, bank.bank_name');
    $this->db->from('payment');
    $querySelector = 'payment.status = 0 OR payment.status = 2 AND payment.payer_id ='.$user_id;
    $this->db->join('user_details', 'user_details.user_id = payment.reciever_id', 'left');
    $this->db->join('user_banks', 'user_details.user_id = user_banks.user_id', 'left');
    $this->db->join('bank', 'bank.bank_id = user_banks.bank_id', 'left');
    $this->db->where($querySelector);
    $details = $this->db->get();
    return $details->result();
  }


  public function confirmPay($payment_id,$trans_type,$user_id,$amount)
  {
    $this->db->trans_start(); # Starting Transaction
    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well
    if ($trans_type == 'received') {
      $data = array(
          'status' => 1,
        );
    }
    elseif ($trans_type == 'paid') {
      $data = array(
          'status' => 2,
        );
    }
    else {
      return 0;
    }
    $this->db->where('payment_id', $payment_id);
    $update = $this->db->update('payment', $data);
    if ($update) {
      $amount *= 2;
      $data = array(
          'user_id' => $user_id,
          'amount' => $amount,
      );
    $confirm_insert = $this->db->insert('comfirm_paid_users', $data);
    $this->db->trans_complete();
      if ($this->db->trans_status() === TRUE) {
         $this->db->trans_commit();
        return true;
      }
      else {
         $this->db->trans_rollback();
        return false;
      }
    }
    else {
      return $update;
    }
  }

  public function pairHelperToRequester($helper_id, $helper_amount, $requester_id, $requester_amount,$helper_db_id, $requester_db_id)
  {
    $this->db->trans_start(); # Starting Transaction
    $this->db->trans_strict(FALSE); # See Note 01. If you wish can remove as well
    $data = array(
        'payer_id' => $helper_id,
        'payer_amount' => $helper_amount,
        'reciever_id' => $requester_id,
        'reciever_amount' => $requester_amount,
      );
      $requester_change = array(
        'paid' => 1,
      );
  $reqCheck = "user_id = ".$requester_id." AND comfirm_paid_users = ".$requester_db_id;
  $this->db->insert('payment', $data);
  $this->db->where($reqCheck);
  $this->db->update('comfirm_paid_users', $requester_change);
  $helper_change = array(
    'status' => 1,
  );
  $HepCheck = "user_id = ".$helper_id." AND help_request_id = ".$helper_db_id;
  $this->db->where($HepCheck);
  $this->db->update('help_request', $helper_change);
  $this->db->trans_complete();
    if ($this->db->trans_status() === TRUE) {
       $this->db->trans_commit();
      return true;
    }
    else {
      $this->db->trans_rollback();
      return false;
    }
  }

  public function totalRecieved($user_id)
  {
    $sql = "SELECT sum(payer_amount) as total_recieved FROM payment WHERE reciever_id = ? AND status = ?";
    $result = $this->db->query($sql, array($user_id, 1));
    // $result = $this->db->get();
    return $result->result()[0];
  }

  public function totalPaid($user_id)
  {
    $sql = "SELECT sum(payer_amount) as total_paid FROM payment WHERE payer_id = ? AND status = ?";
    $result = $this->db->query($sql, array($user_id, 1));
    // $result = $this->db->get();
    return $result->result()[0];
  }

}
?>
